package example.govno3;

import example.govno3.service.SiteParsingService;
import example.govno3.utils.AppProperty;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Govno3ApplicationTests {

    @Autowired
    protected SiteParsingService siteParsingService;

    @Test
    public void getHtmlPageTest() {
        Document doc = siteParsingService.getHtmlPage(AppProperty.getProperty("url"));
        Assert.assertNotNull(doc);
    }

    @Test
    public void getHtmlPageExpectedNullTest() {
        Document doc = siteParsingService.getHtmlPage("https://url.com");
        Assert.assertNull(doc);
    }

    @Test
    public void getHtmlPageExpectedExceptionTest() {
        try {
            siteParsingService.getHtmlPage("url");
            fail();
        } catch (Exception e) {
            Assert.assertTrue(true);
        }
    }

}
